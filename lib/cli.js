const { Command } = require('commander');
const program = new Command();
const fs = require("fs");
const parse = require('.');
program
  .name('cns')
  .description('ContainerScript compiler/executor')
  .version('1.0.0');

program.command('run')
  .description('Run a ContainerScript program')
  .argument('<path>', 'path to program')
  .action((str) => {
      eval(parse(fs.readFileSync(str).toString()).gen);
  });

program.command('compile')
  .description('Compile a ContainerScript program to JavaScript code')
  .argument('<path>', 'path to program')
  .option('--output, -o <path>', 'output of compiled program')
  .action((str, opts) => {
    fs.writeFileSync(opts[0] || str.split('.').slice(0, -1).join('.') + '.js', parse(fs.readFileSync(str).toString()).gen);
  });

program.parse();
