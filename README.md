<img src="images/csl.png" alt="ContainerScript" width="200"/>

A JavaScript variant designed to give you absolute control over the scope (called containers).

Not intended to be a serious project, intended to be a hell to code in (somewhat like an esolang).

# Installation
```
npm i containerscript
```

# Usage
## CLI:
```
cns compile file.cns
```
## API:
```js
const cns = require("containerscript");
const code = `var hello = "Hello, World!";
console.log(hello);`;
const output = cns.generate(
  code
);
console.log(output); // generates output
eval(output) // runs the output
```

# Example
```js
// Create the second and third containers.
createcontainer("second container");
createcontainer("third container");
createcontainer("temporary container");
setcontainer("temporary container");

var v = "hello!";

console.log(v);

deletevariable("v"); // deletes variable from container
deletecontainer("temporary container"); // deletes container
setcontainer("default");

// Define a variable named "str" in the default container.
let str = "Hi!";

// Define a function which showcase ContainerScript's intended usage
function one() {
    console.log(str);
    setcontainer("second container"); // Switch to the second container
    try {
      return console.log(str); // Will error as "str" is not defined in the second container
    } catch(err) {
      console.log("str is not defined in container.");
    }
    // Delete the second container.
    setcontainer("default")
    deletecontainer("second container");
}

one();
```
